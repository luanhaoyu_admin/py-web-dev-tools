# -*- coding: utf-8 -*-
import os
import sys

from pyWebDevTool.util import fileUtil, argumentUtil

if __name__ == "__main__":
    os.chdir(fileUtil.getRootPath())
    if not argumentUtil.has_sys_arg("-bWeb=n"):
        print("cd vuedev")
        os.chdir("vuedev")
        print("npm run build")
        os.system("npm run build")
        print("cd ..")
        os.chdir("..")
    print("rd dist /s /q")
    os.system("rd dist /s /q")
    print("pyinstaller app.spec")
    os.system("pyinstaller app.spec")
    sys.exit(0)
