import {QtRequest} from 'qt-channel'

export const PrivatePyQtRequest = async (options) => {
  options.pyMethod = "qt_private_service"
  options.packageInfo = undefined
  return await QtRequest(options)

}
