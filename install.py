# -*- coding: utf-8 -*-
import os
import sys

from pyWebDevTool.util import fileUtil

if __name__ == "__main__":
    print("begin bo install env")
    os.chdir(fileUtil.getRootPath())
    os.system("python -m pip install --upgrade pip")
    os.system("pip install pipreqs && pip install -r requirements.txt")
    os.chdir("vuedev")
    s = os.system("yarn")
    if s != 0:
        print("yarn is not installed,try npm...")
        ret = os.system("npm install")
        if ret != 0:
            print("npm is not installed, install is stoped,please check")
    print("install finished")
    sys.exit(0)
